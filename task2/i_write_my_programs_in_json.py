import json
import re
number = 0
with open('skychallenge_accounting_input.txt') as f:
    json_string = f.read()
    numbers_in_json = [int(number) for number in re.findall(r'\d+', json_string)]
    sum_of_numbers = sum(numbers_in_json)
    print(sum_of_numbers)
    