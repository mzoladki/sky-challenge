num_of_valid_passwords = 0
with open('skychallenge_skyphrase_input.txt', 'r') as f:
    for line in f:
        if len(line.rstrip().split(' ')) == len(set(line.rstrip().split(' '))):
            num_of_valid_passwords += 1
print(num_of_valid_passwords)